"use strict";

/*
* @Landing js
*/
window.onload = function () {
  $('.landing-loader').fadeOut('slow');
  /*
  *
  * Sliders init
  *
  */

  $('.l-books-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
    nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
  });
  $('.l-bloges-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [{
      breakpoint: 1240,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true
      }
    }, {
      breakpoint: 820,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }],
    prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
    nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
  });
  $('.l-join-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
    nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
  });
  $('.l-promo-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [{
      breakpoint: 1120,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true
      }
    }, {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }],
    prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
    nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
  });
};