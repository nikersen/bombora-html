"use strict";

(function () {
  var WINDOW_WIDTH = window.innerWidth;

  var random = function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  var birthdayCapScene = document.querySelector('.birthday-cap__layers');
  var birthdayCapSceneElements = birthdayCapScene.querySelectorAll('.layer');

  for (var i = 0; i < birthdayCapSceneElements.length; i++) {
    birthdayCapSceneElements[i].dataset.depth = WINDOW_WIDTH >= 800 ? Math.random().toFixed(2) : random(1, 25) * 0.01;
  }

  var birthdayCapParallax = new Parallax(birthdayCapScene);

  var animationEnd = function (el) {
    var animations = {
      animation: 'animationend',
      OAnimation: 'oAnimationEnd',
      MozAnimation: 'mozAnimationEnd',
      WebkitAnimation: 'webkitAnimationEnd'
    };

    for (var t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  }(document.createElement('div'));

  var animationsNames = '.fadeInRightBig, .fadeInDownBig, .fadeInLeftBig';
  var animationElements = document.querySelectorAll('.layer');

  var _loop = function _loop(_i) {
    animationElements[_i].addEventListener(animationEnd, function () {
      var arr = animationsNames.replace(/[.\s]/g, '').split(',');
      arr.forEach(function (name) {
        return animationElements[_i].classList.remove(name);
      });
    });
  };

  for (var _i = 0; _i < animationElements.length; _i++) {
    _loop(_i);
  } // hideCap
  // setTimeout(() => {
  // 	$('.birthday-cap').fadeIn()
  // }, 2000)


  function hideCap() {
    $('.birthday-cap').fadeOut();
  }

  document.addEventListener('keydown', function (e) {
    if (e.keyCode === 27) hideCap();
  });
  document.querySelector('.birthday-cap__back').addEventListener('click', hideCap);

  function success() {
    document.querySelector('.birthday-cap__container form button').classList.add('success');
    document.querySelector('.birthday-cap__container form button p').textContent = 'Отправлено';
  }
})();