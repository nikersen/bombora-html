"use strict";

/**
 * Section: jQuery
 */
$(function () {
  initNavigationUIModule();
  $(window).resize(function () {
    initNavigationUIModule();
  });
  initVacancySlider();
  initTabsUIModule();
  initSectionGeneralSlider();
  initSectionItemsCarousel();
  initSectionBigBannersSlider();
  initMobileNavigationToggle();
  initSearch();
  initPost2();
  initPagesSimpleBar();
  audioControl();
  tabsControl();
  initAccordion();
  productControl();
  $(window).resize(function () {
    initPost2();
  });
});

function initAccordion() {
  // $('#shop-accordion-container').accordion();
  $('.shop-accordion__head').click(function () {
    if ($(this).parent().hasClass('active')) return;
    $('.shop-accordion__item').removeClass('active');
    $('.shop-accordion__body').slideUp();
    $(this).parent().addClass('active');
    $(this).next().slideDown();
  });
}

function initNavigationUIModule() {
  var $menuItemHasChildren = $('.ui.navigation .menu-item-has-children');
  $menuItemHasChildren.off();
  $menuItemHasChildren.click(function () {
    $(this).toggleClass('is-opened');
  });
}

function initTabsUIModule() {
  var $navigationTab = $('.navigation-tabs .navigation-tab');
  $navigationTab.click(function () {
    $(this).parent().children('.is-active').removeClass('is-active');
    $(this).addClass('is-active');
    var $tabsContent = $('.content-tabs[data-tabs-content="' + $(this).parent().data('tabs-content') + '"]');
    $tabsContent.children('.is-active').removeClass('is-active');
    $tabsContent.children('.content-tab').eq($(this).index()).addClass('is-active');
  });
}

function initSectionGeneralSlider() {
  $('.general-slider-slick').each(function () {
    $(this).slick({
      autoplay: true,
      autoplaySpeed: 10000,
      appendArrows: $(this).parent().find('.general-slider-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
    });
  });
}

function initVacancySlider() {
  $('.vacancy-slider').slick({
    autoplay: true,
    appendArrows: $('.vacancy-slider'),
    prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
    nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
  });
}

function initSectionItemsCarousel() {
  $('.items-carousel-slick').each(function () {
    $(this).slick({
      slidesToShow: 5,
      slidesToScroll: 5,
      autoplay: true,
      autoplaySpeed: 10000,
      appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
      responsive: [{
        breakpoint: 1199,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      }, {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }, {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });
  });
  $('.items-carousel-slick-2').each(function () {
    $(this).slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      autoplay: true,
      autoplaySpeed: 7000,
      appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
      responsive: [{
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }, {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });
  });
  $('.items-carousel-slick-3').each(function () {
    $(this).slick({
      slidesToShow: 6,
      slidesToScroll: 6,
      autoplay: true,
      appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
      responsive: [{
        breakpoint: 1199,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      }, {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }, {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });
  });
  $('.events-list--slider').each(function () {
    $(this).slick({
      responsive: [{
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          centerPadding: '15px'
        }
      }, {
        breakpoint: 5000,
        settings: "unslick"
      }]
    });
  });
  $('.items-carousel-slick-4').each(function () {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable: false,
      swipe: false,
      touchMove: false,
      arrows: false,
      asNavFor: '.items-carousel-slick-5'
    });
  });
  $('.items-carousel-slick-5').each(function () {
    $(this).slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      focusOnSelect: true,
      appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>',
      asNavFor: '.items-carousel-slick-4'
    });
  });
  $('.items-carousel-slick-6').each(function () {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
    });
  });
  $('.post-photo-carousel').each(function () {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      appendArrows: $(this).parent().find('.items-carousel-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
    });
  });
}

function initSectionBigBannersSlider() {
  $('.big-banners-slider-slick').each(function () {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      focusOnSelect: true,
      autoplay: true,
      appendArrows: $(this).parent().find('.big-banners-slider-slick-arrows'),
      prevArrow: '<button class="prev"><i class="ion ion-ios-arrow-left"></i></button>',
      nextArrow: '<button class="next"><i class="ion ion-ios-arrow-right"></i></button>'
    });
  });
}

function initMobileNavigationToggle() {
  $('body').on('click', '[data-action="toggle-mobile-navigation"]', function (e) {
    $('section.section.mobile-navigation').toggleClass('is-shown');
    $('html').toggleClass('is-overlay');
  });
}

function initSearch() {
  $('body').on('click', '[data-action="toggle-header-search"]', function (e) {
    $('.section.header .search-form').toggleClass('is-shown');
  });
  $('body').on('click', function (e) {
    if (!$(e.target).closest('.search-form').length && !$(e.target).closest('.search').length) {
      $('.section.header .search-form').removeClass('is-shown');
    }
  });
  $('body').on('focus', '.header .search-form input', function () {
    $('.header .search-form').addClass('is-collapsed');
  });
  $('body').on('focusout', '.header .search-form input', function (e) {
    if ($(e.target).val().length) return false;
    $('.header .search-form').removeClass('is-collapsed');
  });
}

function initPost2() {
  if ($(window).width() >= 768) {
    $('.ui.post.style-full-2 .post-toggle').stick_in_parent({
      offset_top: 60
    });
  }
}

try {
  var gumshoeLinks = document.querySelectorAll('[data-gumshoe] a');

  if (gumshoeLinks.length) {
    new Gumshoe('[data-gumshoe] a', {
      navClass: 'is-active',
      // applied to the nav list item
      offset: 0
    });
  }

  var scroll = new SmoothScroll('a[href*="#"]');
} catch (e) {
  console.log(e);
}
/**
 * Bit Tooltip Cookie
 */


initBitTooltipCookie();

function initBitTooltipCookie() {
  if (!localStorage.getItem('cookie-notified')) {
    $('.cookie-tooltip').show();
  }

  $('[data-action="close-cookie-tooltip"]').click(function () {
    $('.cookie-tooltip').hide();
    localStorage.setItem('cookie-notified', true);
  });
}
/*
*
* @Post media
* Book-pages Simple Scrollbar init
*
*/


function initPagesSimpleBar() {
  var container = $('.js-book-pages-simplescroll');
  var area = document.querySelector('.js-book-pages-simplescroll  .area');
  if (area === null) return;
  var bar = new SimpleBar(area);
  var arrowLeft = container.find('.arrow.left');
  var arrowRight = container.find('.arrow.right');
  arrowRight.on('click', function () {
    $(bar.getContentElement()).animate({
      scrollLeft: bar.getContentElement().scrollLeft + 370
    }, 200);
  });
  arrowLeft.on('click', function () {
    $(bar.getContentElement()).animate({
      scrollLeft: bar.getContentElement().scrollLeft - 370
    }, 200);
  });
}
/*
*
* @Post media
* Audio Control init
*
*/


function audioControl() {
  var music = document.getElementById('sound'); // id for audio element

  var duration; // Duration of audio clip, calculated here for embedding purposes

  var pButton = document.getElementById('pButton'); // play button

  var playhead = document.getElementById('playhead'); // playhead

  var timeline = document.getElementById('timeline'); // timeline

  var viewDuration = document.querySelector('#audioplayer .duration');
  var viewCurrent = document.querySelector('#audioplayer .current');
  if (music === null) return; // timeline width adjusted for playhead

  var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
  $(window).resize(function () {
    timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
  });

  music.onloadeddata = function () {
    viewDuration.innerHTML = convertSeconds(music.duration);
  };

  window.song = music; // play button event listenter

  pButton.addEventListener("click", play); // timeupdate event listener

  music.addEventListener("timeupdate", timeUpdate, false); // makes timeline clickable

  timeline.addEventListener("click", function (event) {
    moveplayhead(event);
    music.currentTime = duration * clickPercent(event);
  }, false); // returns click as decimal (.77) of the total timelineWidth

  function clickPercent(event) {
    return (event.clientX - getPosition(timeline)) / timelineWidth;
  } // makes playhead draggable


  playhead.addEventListener('mousedown', mouseDown, false);
  window.addEventListener('mouseup', mouseUp, false); // Boolean value so that audio position is updated only when the playhead is released

  var onplayhead = false; // mouseDown EventListener

  function mouseDown() {
    onplayhead = true;
    window.addEventListener('mousemove', moveplayhead, true);
    music.removeEventListener('timeupdate', timeUpdate, false);
  } // mouseUp EventListener
  // getting input from all mouse clicks


  function mouseUp(event) {
    if (onplayhead == true) {
      moveplayhead(event);
      window.removeEventListener('mousemove', moveplayhead, true); // change current time

      music.currentTime = duration * clickPercent(event);
      music.addEventListener('timeupdate', timeUpdate, false);
    }

    onplayhead = false;
  } // mousemove EventListener
  // Moves playhead as user drags


  function moveplayhead(event) {
    var newMargLeft = event.clientX - getPosition(timeline);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
      playhead.style.marginLeft = newMargLeft + "px";
    }

    if (newMargLeft < 0) {
      playhead.style.marginLeft = "0px";
    }

    if (newMargLeft > timelineWidth) {
      playhead.style.marginLeft = timelineWidth + "px";
    }

    viewCurrent.innerHTML = convertSeconds(music.currentTime);
  } // timeUpdate
  // Synchronizes playhead position with current point in audio


  function timeUpdate() {
    var playPercent = timelineWidth * (music.currentTime / duration);
    playhead.style.marginLeft = playPercent + "px";

    if (music.currentTime == duration) {
      pButton.className = "";
      pButton.className = "play";
    }

    viewCurrent.innerHTML = convertSeconds(music.currentTime);
  } //Play and Pause


  function play() {
    // update duration
    viewDuration.innerHTML = convertSeconds(music.duration); // start music

    if (music.paused) {
      music.play(); // remove play, add pause

      pButton.className = "";
      pButton.className = "pause";
    } else {
      // pause music
      music.pause(); // remove pause, add play

      pButton.className = "";
      pButton.className = "play";
    }
  } // Gets audio file duration


  music.addEventListener("canplaythrough", function () {
    duration = music.duration;
  }, false); // getPosition
  // Returns elements left position relative to top-left of viewport

  function getPosition(el) {
    return el.getBoundingClientRect().left;
  }

  function convertSeconds(n) {
    var minutes = Math.floor(n / 60);
    var seconds = (n - minutes * 60).toFixed(0);
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return '' + minutes + ':' + seconds;
  }
}
/*
*
* @Ordering
* Tabs init
*
*/


function tabsControl() {
  var tabs = $('.tabs .item');
  tabs.on('click', function () {
    var tab = $('.' + $(this).data('tab'));
    var tabContainer = $('.' + $(this).data('tab-container'));
    tabs.removeClass('active');
    $(this).addClass('active');
    tabContainer.hide();
    tab.show();
  });
}
/*
*
* @Product page
* Product
*
*/


function productControl() {
  var buttons = $('.book-type__btn');
  var audioBtn = $('.book-type__audio-btn');
  var progressBar = $('.round-progress');
  var audio = $('#book-demo')[0];
  productAudioControl();

  function productAudioControl() {
    if (!audio) return;
    audio.addEventListener('timeupdate', function () {
      progressBar.css({
        'stroke-dashoffset': 150 - 150 * (audio.currentTime / audio.duration)
      });
    });
    audioBtn.on('click', function () {
      $('.circle').addClass('show');

      if (audio.paused) {
        $(this).addClass('playing');
        audio.play();
      } else {
        $(this).removeClass('playing');
        audio.pause();
      }
    });
  }
}