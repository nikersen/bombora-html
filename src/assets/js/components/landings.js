(function () {
  /*
  * Slider init
  */

  var speed = 150;
  var carouselClass = "owl-carousel";
  var slider = $(".b2b__main-slider-owl")
  var listLink = $(".b2b__main-list-link")
  var listLinkActive = "b2b__main-list-link_active"

  if (slider.length) {
    slider.trigger("destroy.owl.carousel")
      .removeClass(carouselClass)
      .addClass(carouselClass)
      .owlCarousel({
        smartSpeed: speed * 2,
        dots: false,
        nav: true,
        navClass: ["b2b__main-slider-arrow-left", "b2b__main-slider-arrow-right"],
        navText: ["", ""],
        items: 1,
        slideBy: 1
      })
      .on("translated.owl.carousel", function(e) {
        listLink.removeClass(listLinkActive)
        listLink.eq(e.item.index).addClass(listLinkActive)
      })

    listLink.on("click", function() {
      slider.trigger("to.owl.carousel", [$(this).index()])
      listLink.removeClass(listLinkActive)
      $(this).addClass(listLinkActive)
    })
  }


  $('.faq__trigger').on('click', function () {
    if (!$(this).hasClass('active')) {
      $('.faq__trigger').removeClass('active')
      $('.faq__body').slideUp('fast')

      $(this).addClass('active')
      $(this).closest('.faq__q').find('.faq__body').slideDown('fast')
    } else {
      $('.faq__trigger').removeClass('active')
      $('.faq__body').slideUp('fast')
    }
  })

  $('.landings-nav-trigger').on('click', function () {
    $(this).toggleClass('active')
    $('.landings-nav .container').slideToggle('fast')
  })

})()