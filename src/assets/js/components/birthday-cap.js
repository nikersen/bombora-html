(function () {

	const WINDOW_WIDTH = window.innerWidth;
	const random = function random(min, max) {
	    return Math.floor(Math.random() * (max - min + 1) + min);
	}

	const birthdayCapScene = document.querySelector('.birthday-cap__layers')
	const birthdayCapSceneElements = birthdayCapScene.querySelectorAll('.layer')

	for (let i = 0; i < birthdayCapSceneElements.length; i++) {
		birthdayCapSceneElements[i].dataset.depth = WINDOW_WIDTH >= 800 
			? Math.random().toFixed(2) 
			: random(1, 25) * 0.01
	}

	const birthdayCapParallax = new Parallax(birthdayCapScene)




	const animationEnd = function(el) {
	    const animations = {
	        animation: 'animationend',
	        OAnimation: 'oAnimationEnd',
	        MozAnimation: 'mozAnimationEnd',
	        WebkitAnimation: 'webkitAnimationEnd'
	    };
	    for (const t in animations) {
	        if (el.style[t] !== undefined) {
	            return animations[t];
	        }
	    }
	}(document.createElement('div'))
	const animationsNames = '.fadeInRightBig, .fadeInDownBig, .fadeInLeftBig'
	const animationElements = document.querySelectorAll('.layer')

	for (let i = 0; i < animationElements.length; i++) {
	    animationElements[i].addEventListener(animationEnd, function() {
	        const arr = animationsNames.replace(/[.\s]/g, '').split(',')
	        arr.forEach(function(name) {
	            return animationElements[i].classList.remove(name)
	        })
	    })
	}


	// hideCap
	// setTimeout(() => {
	// 	$('.birthday-cap').fadeIn()
	// }, 2000)

	function hideCap () {
		$('.birthday-cap').fadeOut()
	}

	document.addEventListener('keydown', (e) => {
		if (e.keyCode === 27) hideCap()
	})

	document.querySelector('.birthday-cap__back')
		.addEventListener('click', hideCap)


	function success () {
		document.querySelector('.birthday-cap__container form button').classList.add('success')
		document.querySelector('.birthday-cap__container form button p').textContent = 'Отправлено'
	}

})();