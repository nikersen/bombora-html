/**
 * General variables
 */

const { src, dest, watch, parallel, series } = require('gulp')

const babel 		= require('gulp-babel')
const csso 			= require('gulp-csso')
const rename 		= require('gulp-rename')
const browserSync 	= require('browser-sync')
const uglify 		= require('gulp-uglify')
const rimraf 		= require('rimraf')
const autoprefixer 	= require('gulp-autoprefixer')
const fileInclude 	= require('gulp-file-include')
const argv 			= require('yargs').argv
const sass 			= require('gulp-sass')
const zip 			= require('gulp-zip')
const sourcemaps 	= require('gulp-sourcemaps')
const runSequence 	= require('run-sequence')
const del 			= require('del')


const srcPath  = './src'
const devPath  = './dev'
const distPath = './dist'

const devBuild = argv.dev || argv.disableOptimize

/**
 * Browser Sync Init
 */
function browser_sync () {
	browserSync.init({
		server: {
			baseDir: devPath,
			// index: 'index.html',
			directory: true // or index: "index.html"
		},
		port: 4122
	})
}
exports.browser_sync = browser_sync

/**
 * Build styles
 */

function build_styles () {
	if (devBuild) {
		return src(srcPath + '/assets/css/main.scss')
			.pipe(sass().on('error', onError))
			.pipe(rename({ suffix: '.min', prefix: '' }))
			.pipe(dest(devPath + '/assets/css'))
			.pipe(browserSync.stream())
	}

	return src(srcPath + '/assets/css/main.scss')
		.pipe(fileInclude('//@@'))
		.pipe(sass().on('error', onError))
		.pipe(autoprefixer({ cascade: false }))
		.pipe(dest(distPath + '/assets/css'))
		.pipe(csso({
			comments: false
		}))
		.pipe(rename({ suffix: '.min', prefix: '' }))
		.pipe(dest(distPath + '/assets/css'))
		.pipe(browserSync.stream())
}
exports.styles = build_styles
exports.build_styles = build_styles


/**
 * Build scripts
 */

function build_scripts () {
	if (devBuild) {
		return src(srcPath + '/assets/js/**/*.js')
			.pipe(rename({ suffix: '.min', prefix: '' }))
			.pipe(fileInclude('//@@'))
			.on('error', onError)
			.pipe(dest(devPath + '/assets/js'))
	}
	return src(srcPath + '/assets/js/**/*.js')
		.pipe(fileInclude('//@@'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
		.pipe(dest(distPath + '/assets/js'))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min', prefix: '' }))
		.on('error', onError)
		.pipe(dest(distPath + '/assets/js'))
}
exports.scripts = build_scripts
exports.build_scripts = build_scripts


/**
 * Copy img
 */

function copy_img () {
	if (devBuild) {
		return src(srcPath + '/assets/img/**/*.*')
			.pipe(dest(devPath + '/assets/img'))
			.pipe(browserSync.reload({stream: true}))
	}
	return src(srcPath + '/assets/img/**/*.*')
		.pipe(dest(distPath + '/assets/img'))
		.pipe(browserSync.reload({stream: true}))
}
exports.images = copy_img
exports.img = copy_img
exports.copy_img = copy_img


/**
 * Copy assets
 */
function copy_assets () {
	if (devBuild) {
		return (
			src(srcPath + '/assets/font/**')
				.pipe(dest(devPath + '/assets/font')),
			src(srcPath + '/assets/img/**')
				.pipe(dest(devPath + '/assets/img')),
			src(srcPath + '/assets/php/**')
				.pipe(dest(devPath + '/assets/php')),
			src(srcPath + '/assets/vendor/**')
				.pipe(dest(devPath + '/assets/vendor'))
		)
	}
	return (
		src(srcPath + '/assets/font/**')
			.pipe(dest(distPath + '/assets/font')),
		src(srcPath + '/assets/img/**')
			.pipe(dest(distPath + '/assets/img')),
		src(srcPath + '/assets/php/**')
			.pipe(dest(distPath + '/assets/php')),
		src(srcPath + '/assets/vendor/**')
			.pipe(dest(distPath + '/assets/vendor'))
	)
}
exports.copy = copy_assets
exports.assets = copy_assets
exports.copy_assets = copy_assets


/**
 * Build templates
 */

function build_templates () {
	if (devBuild) {
		return src(srcPath + '/templates/**/*.html')
			.pipe(fileInclude({
				prefix: '//@@',
				basepath: '@file'
			}))
			.on('error', onError)
			.pipe(dest(devPath))
	}
	return src(srcPath + '/templates/**/*.html')
		.pipe(fileInclude({
			prefix: '//@@',
			basepath: '@file'
		}))
		.on('error', onError)
		.pipe(dest(distPath + '/templates/'))
}
exports.templates = build_templates
exports.build_templates = build_templates


/**
 * Build vendor scripts
 */

function build_scripts_vendor () {
	if (devBuild) {
		return src([srcPath + '/assets/js/vendor.js'])
			.pipe(rename({ suffix: '.min', prefix: '' }))
			.pipe(fileInclude('//@@'))
			.on('error', onError)
			.pipe(dest(devPath + '/assets/js'))
	}
	return src([srcPath + '/assets/js/vendor.js'])
		.pipe(rename({ suffix: '.min', prefix: '' }))
		.pipe(fileInclude('//@@'))
		.pipe(uglify())
		.on('error', onError)
		.pipe(dest(distPath + '/assets/js'))
}
exports.vendor = build_scripts_vendor
exports.build_scripts_vendor = build_scripts_vendor


/**
 * Reload browser
 */

function browser_reload (done) {
	browserSync.reload()

	done()
}
exports.browser_reload = browser_reload


/**
 * General watcher
 */

function watcher () {
	watch([srcPath + '/**/*.scss', srcPath + '/**/*.css'], build_styles)
	watch([srcPath + '/assets/img/**/*.*'], copy_img)
	watch([srcPath + '/assets/js/*.js'], series(build_scripts, browser_reload))
	watch([srcPath + '/assets/vendor/**/*.js', srcPath + '/assets/js/vendor.js'], series(build_scripts_vendor, browser_reload))
	watch(srcPath + '/**/*.html', series(build_templates, browser_reload))
}
exports.watcher = watcher


/**
 * Clean dist
 */

function clean_dist (cb) {
	// del.sync(distPath + '/**', { force: true })
	rimraf(distPath + '/**', cb)
}
exports.clean_dist = clean_dist


/**
 * Clean dev
 */

function clean_dev (cb) {
	// del.sync(devPath + '/**', { force: true })
	rimraf(devPath + '/**', cb)
}
exports.clean_dev = clean_dev


// Error handler

function onError(err) {
	console.log(err)
	this.emit('end')
}

/**
 * Gulp Start
 */

exports.build = series(clean_dist, clean_dev, parallel(build_styles, build_scripts, build_scripts_vendor, build_templates, copy_assets), copy_assets)
exports.default = series(clean_dev, parallel(build_styles, build_scripts, build_scripts_vendor, build_templates), parallel(watcher, browser_sync, copy_assets))